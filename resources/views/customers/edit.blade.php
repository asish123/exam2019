@extends('layouts.app')
@section('content')

<h1> Edit A Customer </h1>
<form method = 'post' action = "{{action('CustomerController@update',$customer->id)}}">
@csrf
@method('PATCH')

<div class="form-group">
<label for = "title">Name:</label>
<input type = "text" class = "form-control" name= "customername" value="{{$customer->customername}}">
<label for = "title">Email:</label>
<input type = "text" class = "form-control" name= "email" value="{{$customer->email}}">
<label for = "title">Phone:</label>
<input type = "text" class = "form-control" name= "phone" value="{{$customer->phone}}">

</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>

</form>
@endsection