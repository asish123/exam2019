@extends('layouts.app')
@section('content')


<h1>This is the customers list</h1>
<table>
 <a href = "{{route('customers.create') }}"> <h5> add a new customer </h5> </a>
    <tr>
     <th>ID</th>
     <th>Name</th>
     <th>Email</th> 
     <th>Phone</th>
    </tr>

    @foreach($customers as $customer)
    <tr>
        <td>{{$customer->id}}&nbsp;&nbsp; </td> 
        <td>{{$customer->customername}} &nbsp;</td> 
        <td>{{$customer->email}} &nbsp;</td> 
        <td>{{$customer->phone}} &nbsp;</td> 
        <td><a href ="{{route('customers.edit', $customer ->id)}}"> Edit &nbsp; </a> </td>
        <td> @can('manager') <a href="{{route('delete', $customer ->id)}}"> @endcan Delete</a> &nbsp; </td>
       
   @endforeach

</table>
@endsection