<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                  'username'=>'a',
                  'email'=>'a@a.com',
                  'password'=>Hash::make('12345678'),
                  'phone' => '0508156247',
                  'created_at' =>date('y-m-d G:i:s'),
                  'role' => 'manager'  
            ],
            [
                'username'=>'b',
                'email'=>'b@b.com',
                'password'=>Hash::make('12345678'),
                'phone' => '0505476247',
                'created_at' =>date('y-m-d G:i:s'),  
                'role' => 'salesrep' 
          ],
          [
            'username'=>'c',
            'email'=>'c@c.com',
            'password'=>Hash::make('12345678'),
            'phone' => '0505476995',
            'created_at' =>date('y-m-d G:i:s'),
            'role' => 'salesrep'   
      ],

            ] );

    }
}
