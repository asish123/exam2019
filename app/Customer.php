<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable =[
    'customername' ,'email','phone'
   ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
