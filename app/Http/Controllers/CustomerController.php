<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
    $id = Auth::id();
    $user =User::find($id);
    $customers =Customer::all();
    return view('customers.index',['customers' => $customers]); }
    return redirect()->intended('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
        return view('customers.create'); }

        return redirect()->intended('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
        $customer = new Customer();
        $id=Auth::id();
        $customer->user_id = $id; 
        $customer->customername = $request->customername;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
       $customer->save(); 
        return redirect('customers'); }
        return redirect()->intended('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
        $customer = Customer::find($id);
        return view('customers.edit', ['customer' => $customer]);
        }
        return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
        $customer = Customer::find($id);
        $customer->update($request -> all());
       return redirect('customers');
        }
        return redirect()->intended('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to delete customers");
         }          
        if (Auth::check()) {
        $customer = Customer::find($id);
        $customer->delete(); 
        return redirect('customers'); 
        }
        return redirect()->intended('/home');
    }
}
